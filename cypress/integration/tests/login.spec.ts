describe("Login view", () => {
  it("Should show an error message when the credentials are not valid", () => {
    cy.visit("http://localhost:8080/backoffice/login");
    const username = "test@test.uy";
    const password = "test";
    cy.get("input[type='text']")
      .type(username, { delay: 100 })
      .should("have.value", username);
    cy.get("input[type='password']")
      .type(password, { delay: 100 })
      .should("have.value", password);
    cy.get("button").click();
    cy.get(".notification-content").should("have.text", "Credenciales Incorrectas");
  });

  it("Should show rules messages when no username or password", () => {
    cy.visit("http://localhost:8080/backoffice/login");
    cy.get("button").click();
    cy.get(".v-messages__message").each(($el, index) => {
      if (index == 0) expect($el[0].textContent).equal("Ingrese un email valido");
      if (index == 1) expect($el[0].textContent).equal("Este campo es obligatorio");
    });
  });

  it("Should do login and redirect to the /", () => {
    cy.visit("http://localhost:8080/backoffice/login");
    const username = "admin@udelar.uy";
    const password = "admin";
    cy.get("input[type='text']")
      .type(username, { delay: 100 })
      .should("have.value", username);
    cy.get("input[type='password']")
      .type(password, { delay: 100 })
      .should("have.value", password);
    cy.get("button")
      .click()
      .then(() => {
        cy.url().should("eq", "http://localhost:8080/backoffice/login");
      });
  });
});
