import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "@mdi/font/css/materialdesignicons.css";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#2196F3",
        secondary: "#00bcd4",
        accent: "#82B1FF",
        error: "#f76358",
        info: "#2196F3",
        success: "#619d4b",
        warning: "#f9a825"
      }
    }
  },
  icons: {
    iconfont: "mdi"
  }
});
