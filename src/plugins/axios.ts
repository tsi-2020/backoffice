import axios from "axios";
import { getNewToken } from "@/services/auth";

const config = {
  baseURL: process.env.baseURL || process.env.apiURL || "https://udelar.web.elasticloud.uy"
};

const instance = axios.create(config);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function validateTokenRequest(error: any) {
  if (error.config.url.includes("login") || error.response.status !== 401) {
    throw error;
  }
}

export function updateHeaders(token: string) {
  instance.defaults.headers.common["Authorization"] = `Bearer ${token}`;
}

instance.interceptors.request.use(request => {
  if (!instance.defaults.headers.common["Authorization"]) {
    const tokens: { accessToken: string; refreshToken: string } | null = JSON.parse(
      localStorage.getItem("adminTokens") || "null"
    );
    updateHeaders(tokens?.accessToken ?? "");
  }
  return request;
});

instance.interceptors.response.use(
  response => response,
  async error => {
    validateTokenRequest(error);
    if (error.config.url == "/api/Auth/refreshToken") {
      localStorage.removeItem("admin");
      localStorage.removeItem("adminTokens");
      throw error;
    }

    try {
      const data = await getNewToken();
      const config = error.config;
      config.headers["Authorization"] = `Bearer ${data.accessToken}`;
      updateHeaders(data.accessToken);
      return instance.request(config);
    } catch (_err) {
      throw error;
    }
  }
);

export default instance;
