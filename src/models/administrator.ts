export type Administrator = {
  name: string;
  firstName: string;
  lastName: string;
  id: string;
  password: string;
  email: string;
};

export type AdministratorPassword = {
  id: string;
  newPassword: string;
};

export type AdministratorUpdate = {
  id: string;
  firstName: string;
  lastName: string;
};
