export type CreateCourse = {
  name: string;
  responsibleTeacherId?: string;
  subjectTemplateId: string;
  validateWithBedelia: boolean;
  isRemote: boolean;
};

export type Course = {
  id: string;
  name: string;
  validateWithBedelia: boolean;
  isRemote: boolean;
};
