export type Report = {
  rows: [
    {
      id: string;
      name: string;
      professorsQuantity: number;
      studentsQuantity: number;
      universityName?: string;
    }
  ];
};
