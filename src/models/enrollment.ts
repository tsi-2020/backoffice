export type ProfessorEnrollment = {
  userId: string;
  isResponsible: boolean;
};

export type Enrollment = {
  id: string;
  user: {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    ci: 0;
    roles: string[];
  };
  isResponsible: true;
};
