export type Poll = {
  id: string;
  title: string;
  createdOn: string;
};

export type NewPollType = {
  title: string;
  openQuestions: Array<{ text: string }>;
  multipleChoiceQuestions: Array<{ text: string; isMultipleSelect: boolean; choices: Array<{ text: string }> }>;
};

export type PollAnswer = {
  title: string;
  createdOn: string;
  openAnswerDtos: Array<{ question: string; position: number; response: string }>;
  multipleChoiceAnswerDtos: Array<{
    question: string;
    position: number;
    choices: Array<string>;
  }>;
};
