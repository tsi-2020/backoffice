export type University = {
  id: string;
  name: string;
  accessPath: string;
  administrators?: [];
  description: string;
  logo?: string;
  primaryColor?: string;
  secondaryColor?: string;
};
