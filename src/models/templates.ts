export type Template = {
  id: string;
  title: string;
  description: string;
  educationalUnits: [
    {
      id: string;
      position: number;
      title: string;
      text: string;
      assigmentComponents: [
        {
          id: string;
          position: number;
          title: string;
          text: string;
          assignmentId: string;
        }
      ];
      documentComponents: [
        {
          id: string;
          position: number;
          title: string;
          url: string;
        }
      ];
      forumComponents: [
        {
          id: string;
          title: string;
          position: number;
          forumId: string;
        }
      ];
      linkComponents: [
        {
          id: string;
          position: number;
          title: string;
          url: string;
        }
      ];
      pollComponents: [
        {
          id: string;
          position: number;
          title: string;
          text: string;
          pollId: string;
        }
      ];
      quizComponents: [
        {
          id: string;
          position: number;
          title: string;
          text: string;
          quizId: string;
        }
      ];
      textComponents: [
        {
          id: string;
          position: number;
          text: string;
        }
      ];
      videoComponents: [
        {
          id: string;
          position: number;
          title: string;
          url: string;
        }
      ];
      virtualMeetingComponents: [
        {
          id: string;
          position: number;
          title: string;
          url: string;
        }
      ];
    }
  ];
};
