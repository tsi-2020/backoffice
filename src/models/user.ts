export type User = {
  id?: string;
  firstName: string;
  lastName: string;
  email: string;
  ci: string;
  roles?: string[];
};
