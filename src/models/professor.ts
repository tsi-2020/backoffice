export type Professor = {
  id?: string;
  firstName: string;
  lastName: string;
  email: string;
  password?: string;
  ci: string;
  roles?: string[];
};
