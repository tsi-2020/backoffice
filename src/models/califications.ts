export type Calification = {
  student: {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    ci: 0;
    roles: [string];
  };
  score: 0;
};
