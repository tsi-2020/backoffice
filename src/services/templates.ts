import axios from "@/plugins/axios";
import { Template } from "@/models/templates";

export async function getTemplates(): Promise<Template[]> {
  const response = await axios.get<Template[]>("/api/Templates");
  return response.data;
}

export async function getTemplate(templateId: string): Promise<Template> {
  const response = await axios.get<Template>(`/api/Templates/${templateId}`);
  return response.data;
}
