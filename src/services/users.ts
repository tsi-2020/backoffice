import axios from "@/plugins/axios";
import { User } from "@/models/user";

export async function createUser(user: User): Promise<User> {
  const response = await axios.post<User>("/api/Users", user);
  return response.data;
}

export async function getUsers(filters: {
  page: number;
  limit: number;
  user: string;
}): Promise<{ count: number; userList: User[] }> {
  let params;
  if (filters.user) {
    params = { Page: filters.page, Limit: filters.limit, FullNameFilter: filters.user };
  } else {
    params = { Page: filters.page, Limit: filters.limit };
  }
  const response = await axios.get<{ count: number; userList: User[] }>("/api/Users", {
    params: params
  });
  return response.data;
}

export async function removeUser(userId: string) {
  await axios.delete(`/api/Users/${userId}`);
}
