import axios from "@/plugins/axios";
import { Calification } from "@/models/califications";

export async function getCalifications(subjectId: string): Promise<Calification[]> {
  const response = await axios.get<Calification[]>(`/api/Subjects/${subjectId}/Califications`);
  return response.data;
}

export async function closeCalifications(subjectId: string): Promise<Calification[]> {
  const response = await axios.post<Calification[]>(`/api/Subjects/${subjectId}/Califications`);
  return response.data;
}
