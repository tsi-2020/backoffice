import axios from "@/plugins/axios";
import { CreateCourse, Course } from "@/models/course";

export async function getCourses(universityId: string): Promise<Course[]> {
  const response = await axios.get<Course[]>(`/api/Universities/${universityId}/Subjects`);
  return response.data;
}

export async function createCourse(course: CreateCourse): Promise<Course> {
  const response = await axios.post<Course>("/api/Subjects", course);
  return response.data;
}

export async function deleteCourse(courseId: string): Promise<void> {
  return await axios.delete(`/api/Subjects/${courseId}`);
}

export async function updateCourse(
  courseId: string,
  course: { name: string; validateWithBedelia: boolean; isRemote: boolean }
): Promise<void> {
  await axios.put(`/api/Subjects/${courseId}`, course);
}
