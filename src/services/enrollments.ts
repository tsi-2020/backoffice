import axios from "@/plugins/axios";
import { ProfessorEnrollment, Enrollment } from "@/models/enrollment";

export async function enrollProfessors(subjectId: string, professors: ProfessorEnrollment[]): Promise<void> {
  await axios.post<void>(`/api/Subjects/${subjectId}/Professors`, professors);
}

export async function getEnrollments(subjectId: string): Promise<Enrollment[]> {
  const response = await axios.get<{ professors: Enrollment[] }>(`/api/Subjects/${subjectId}/Enrollments`);
  return response.data.professors;
}
