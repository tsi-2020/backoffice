import axios from "@/plugins/axios";
import { Report } from "@/models/reports";

export async function getSubjectEnrollments(): Promise<Report> {
  const response = await axios.get<Report>("/api/Reports/SubjectEnrollments");
  return response.data;
}

export async function getUniversityUsers(): Promise<Report> {
  const response = await axios.get<Report>("/api/Reports/UniversityUsers");
  return response.data;
}
