import axios from "@/plugins/axios";
import { Administrator, AdministratorPassword, AdministratorUpdate } from "../models/administrator";

export async function createAdministrator(administrator: Administrator, universityId: string): Promise<Administrator> {
  const response = await axios.post<Administrator>(
    `/api/Universities/${universityId}/UniversityAdministrators`,
    administrator
  );
  return response.data;
}

export async function updateAdministrator(administrator: AdministratorUpdate, universityId: string): Promise<void> {
  return axios.put(`/api/Universities/${universityId}/UniversityAdministrators/${administrator.id}`, administrator);
}

export async function getAdministrators(): Promise<Administrator[]> {
  const response = await axios.get<Administrator[]>("/api/UniversityAdministrators");
  return response.data;
}

export async function getAdministrator(administratorId: string): Promise<Administrator> {
  const response = await axios.get<Administrator>(`/api/UniversityAdministrators/${administratorId}`);
  return response.data;
}

export async function changeAdministratorPassword(
  administrator: AdministratorPassword,
  universityId: string
): Promise<void> {
  return axios.patch(
    `/api/Universities/${universityId}/UniversityAdministrators/${administrator.id}/password`,
    administrator
  );
}

export async function deleteAdministrator(administratorId: string, universityId: string): Promise<void> {
  return await axios.delete(`/api/Universities/${universityId}/UniversityAdministrators/${administratorId}`);
}
