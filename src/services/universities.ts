import axios from "@/plugins/axios";
import { University } from "@/models/university";

export async function getUniversities(): Promise<University[]> {
  const response = await axios.get<University[]>("/api/Universities");
  return response.data;
}

export async function getUniversity(universityId: string): Promise<University> {
  const response = await axios.get<University>(`/api/Universities/${universityId}`);
  return response.data;
}

export async function createUniversity(university: University): Promise<University> {
  const response = await axios.post<University>("/api/Universities", university);
  return response.data;
}

export async function updateUniversity(university: University): Promise<void> {
  await axios.put<University>("/api/Universities", university);
}

export async function deleteUniversity(universityId: string): Promise<void> {
  await axios.delete("/api/Universities/" + universityId);
}
