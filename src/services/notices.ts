import axios from "@/plugins/axios";

export async function createNotice(
  universityId: string,
  courseId: string,
  notice: { message: string; title: string }
): Promise<void> {
  await axios.post(`/api/Universities/${universityId}/Subjects/${courseId}/Notices`, notice);
}

export async function createNoticeInUniversity(
  universityId: string,
  notice: { message: string; title: string }
): Promise<void> {
  await axios.post(`/api/Universities/${universityId}/Notices`, notice);
}
