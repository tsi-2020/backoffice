import axios from "@/plugins/axios";
import { Career } from "@/models/career";

export async function getCareers(universityId: string) {
  const response = await axios.get<Career[]>(`/api/Universities/${universityId}/Careers`);
  return response.data;
}

export async function getCareer(careerId: string, universityId: string) {
  const response = await axios.get<Career>(`/api/Universities/${universityId}/Careers/${careerId}`);
  return response.data;
}

export async function createCareer(name: string): Promise<Career> {
  const response = await axios.post<Career>("/api/Careers", { name: name });
  return response.data;
}

export async function updateCareer(careerId: string, name: string): Promise<void> {
  await axios.put("/api/Careers", { id: careerId, name: name });
}

export async function deleteCareer(careerId: string): Promise<void> {
  await axios.delete(`/api/Careers/${careerId}`);
}

export async function addCoursesToCareer(careerId: string, courses: string[]): Promise<void> {
  await axios.post(`/api/Careers/${careerId}/Subjects`, courses);
}

export async function removeCoursesFromCareer(careerId: string, courses: string[]): Promise<void> {
  await axios.patch(`/api/Careers/${careerId}/Subjects`, courses);
}
