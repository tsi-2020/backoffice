import axios from "@/plugins/axios";
import store from "@/store";
import jwtDecode from "jwt-decode";

export async function authenticate(user: {
  email: string;
  password: string;
}): Promise<{ accessToken: string; refreshToken: string }> {
  const response = await axios.post("/api/Auth/login", user);
  return response.data;
}

export async function getNewToken(): Promise<{ accessToken: string; refreshToken: string }> {
  const data = localStorage.getItem("adminTokens");
  const tokens: { accessToken: string; refreshToken: string } = JSON.parse(data || "null");
  if (tokens) {
    const response = await axios.post("/api/Auth/refreshToken", {
      refreshToken: tokens.refreshToken
    });
    store.commit("SET_USER_DATA", jwtDecode(response.data.accessToken));
    store.commit("SET_USER_TOKENS", response.data);
    return response.data;
  } else {
    throw new Error("Missing credentials");
  }
}
