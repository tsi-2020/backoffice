import axios from "@/plugins/axios";
import { Professor } from "@/models/professor";

export async function getProfessor(): Promise<Professor[]> {
  const response = await axios.get<{ count: number; userList: Professor[] }>("/api/Users", {
    params: { RoleFilter: "Professor" }
  });
  return response.data.userList;
}

export async function createProfessor(professor: Professor): Promise<Professor> {
  const response = await axios.post<Professor>("/api/Professors", professor);
  return response.data;
}

export async function updateProfessor(
  professorId: string,
  professor: { firstName: string; lastName: string; email: string }
): Promise<void> {
  await axios.put(`/api/Users/${professorId}`, professor);
}

export async function removeRole(professorId: string, roles: string[]): Promise<void> {
  await axios.patch(`/api/Users/${professorId}/Roles`, { roles: roles });
}
