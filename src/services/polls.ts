import axios from "@/plugins/axios";
import { NewPollType, Poll, PollAnswer } from "@/models/poll";

export async function createPoll(universityId: string, poll: NewPollType): Promise<void> {
  await axios.post(`/api/Universities/${universityId}/Polls`, poll);
}

export async function createPollUdelar(poll: NewPollType): Promise<void> {
  await axios.post(`/api/Polls`, poll);
}

export async function publishPoll(universityId: string, subjectId: string, pollId: string): Promise<void> {
  await axios.post(`/api/Universities/${universityId}/Subjects/${subjectId}/Polls/${pollId}`);
}

export async function publishPollInUniversity(universityId: string, pollId: string): Promise<void> {
  await axios.post(`/api/Universities/${universityId}/Polls/${pollId}`);
}

export async function getPolls(): Promise<Poll[]> {
  const response = await axios.get<Poll[]>("/api/Polls");
  return response.data;
}

export async function getPollAnwers(
  pollId: string,
  filters: { page: number; limit: number }
): Promise<{ count: number; readPollAnswerDtos: PollAnswer[] }> {
  const response = await axios.get<{ count: number; readPollAnswerDtos: PollAnswer[] }>(
    `/api/Polls/${pollId}/Answers`,
    { params: { page: filters.page, limit: filters.limit } }
  );
  return response.data;
}
