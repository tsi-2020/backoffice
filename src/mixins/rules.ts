import { Vue, Component } from "vue-property-decorator";

@Component({})
export class RuleMixin extends Vue {
  public rules = {
    required: (value: string) => !!value || "Este campo es obligatorio",
    path: (value: string) => {
      const pattern = /^\/[a-z]{4,15}$/;
      return pattern.test(value) || "Ingrese una direccion valida";
    },
    mail: (value: string) => {
      const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return pattern.test(value) || "Ingrese un email valido";
    },
    minPassword: (v: string) => v.length >= 8 || "Minimo 8 caracteres",
    fileSize: (value: File) => !value || value.size < 1000000 || "El tamaño de la imagen debe ser menor a 1 MB",
    selectRule: (v: []) => !v || v.length >= 1 || "Este campo es requerido"
  };

  public onlyNumber(event: KeyboardEvent) {
    const pattern = /[0-9]/;
    if (!pattern.test(event.key)) {
      event.preventDefault();
    }
  }
}
