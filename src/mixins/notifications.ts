import { Vue, Component } from "vue-property-decorator";
import { AxiosError } from "axios";

@Component({})
export class NotificationMixin extends Vue {
  public success(title: string, text: string) {
    this.$notify({
      group: "feedback",
      duration: 5000,
      title: title,
      text: text,
      type: "success"
    });
  }

  public error(title: string, text: string) {
    this.$notify({
      group: "feedback",
      duration: 5000,
      title: title,
      text: text,
      type: "error"
    });
  }

  public errorHandling(error: AxiosError) {
    // debugger
    if (error.isAxiosError && error.response) {
      if (error.response.status === 500) {
        this.$router.push({ name: "Server Error" });
        this.error("Error", "Oops algo salio mal :(");
      } else {
        this.error("Error", error.response.data.message);
      }
    } else {
      this.$router.push({ name: "Server Error" });
      this.error("Error", "Oops algo salio mal :(");
    }
  }
}
