import { Course } from "@/models/course";
import { Professor } from "@/models/professor";
import { Administrator } from "@/models/administrator";
import { University } from "@/models/university";
import { Template } from "@/models/templates";
import { Poll } from "@/models/poll";
import { Career } from "@/models/career";
import { Vue, Component } from "vue-property-decorator";

@Component({})
export class SearchMixin extends Vue {
  private toLocalDate(date: string) {
    const options = { weekday: "long", year: "numeric", month: "long", day: "numeric" };
    const localDate = new Date(date).toLocaleDateString("es-ES", options);
    return localDate;
  }

  private filterProfessorOrAdministrator(item: Professor | Administrator): boolean {
    return (
      (item as Administrator | Professor).firstName.toLowerCase().includes(this.$store.state.search.toLowerCase()) ||
      (item as Administrator | Professor).lastName.toLowerCase().includes(this.$store.state.search.toLowerCase()) ||
      (item as Administrator | Professor).email.toLowerCase().includes(this.$store.state.search.toLowerCase())
    );
  }

  private filterPoll(item: Poll): boolean {
    return (
      item.title.toLowerCase().includes(this.$store.state.search.toLowerCase()) ||
      this.toLocalDate(item.createdOn)
        .toLowerCase()
        .includes(this.$store.state.search.toLowerCase())
    );
  }

  private filterTemplate(item: Template): boolean {
    return (
      item.title.toLowerCase().includes(this.$store.state.search.toLowerCase()) ||
      item.description.toLowerCase().includes(this.$store.state.search.toLowerCase())
    );
  }

  public filterData(items: Array<Administrator | Professor | Course | University | Poll | Template | Career>) {
    return items.filter(item => {
      if ("name" in item) {
        return item.name.toLowerCase().includes(this.$store.state.search.toLowerCase());
      }
      if ("email" in item) {
        return this.filterProfessorOrAdministrator(item);
      }
      if ("description" in item) {
        return this.filterTemplate(item);
      }
      if ("title" in item) {
        return this.filterPoll(item);
      }
    });
  }
}
