import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import UserModule from "@/store/modules/user";
import store from "@/store/index";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/",
    component: () => {
      return UserModule.userRole == "UdelarAdmin"
        ? import("../views/Universities/Universities.vue")
        : import("../views/Administrators.vue");
    },
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/universities",
    name: "Universities",
    component: () => import("../views/Universities/Universities.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/university/:id",
    name: "University",
    component: () => import("../views/Universities/University.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/administrators",
    name: "Administrators",
    component: () => import("../views/Administrators.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/users",
    name: "Users",
    component: () => import("../views/Professors.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/careers",
    name: "Careers",
    component: () => import("../views/Careers.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/courses",
    name: "Courses",
    component: () => import("../views/Courses.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/polls",
    name: "Polls",
    component: () => import("../views/Polls/Polls.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/add-poll",
    name: "Add Poll",
    component: () => import("../views/Polls/AddPoll.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/polls/:poll/answers",
    name: "PollAnswers",
    component: () => import("@/views/Polls/PollAnswers.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: true
    }
  },
  {
    path: "/templates",
    name: "Templates",
    component: () => import("../views/Templates.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/reports",
    name: "Reports",
    component: () => import("../views/Reports.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/bedelias",
    name: "Bedelias",
    component: () => import("../views/Bedelias.vue"),
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/error",
    name: "Server Error",
    component: () => import("../views/Errors/ServerError.vue"),
    meta: {
      requireAuth: false
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, _from, next) => {
  store.commit("SET_IN_LOGIN", to.name === "Login");
  const loggedIn = localStorage.getItem("admin");
  if (to.matched.some(record => record.meta.requireAuth) && !loggedIn) {
    next("/login");
  } else if (loggedIn && to.name == "Login") {
    next("/");
  }
  next();
});

export default router;
