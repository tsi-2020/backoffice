import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    mini: true,
    search: ""
  },
  mutations: {
    SET_MINI(state, value) {
      state.mini = value;
    },
    SET_SEARCH(state, value) {
      state.search = value;
    }
  }
});
