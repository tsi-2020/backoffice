import { getModule, Module, VuexModule, Mutation, Action } from "vuex-module-decorators";
import store from "@/store";

type UserType = {
  Tenant: string;
  email: string;
  exp: number;
  given_name: string;
  iat: number;
  nameid: string;
  nbf: number;
  role: string;
};

@Module({ dynamic: true, store, name: "users" })
class User extends VuexModule {
  inLogin = true;
  userTokens: { accessToken: string; refreshToken: string } | null = JSON.parse(
    localStorage.getItem("adminTokens") || "null"
  );
  user: UserType | null = JSON.parse(localStorage.getItem("admin") || "null");

  @Mutation
  SET_IN_LOGIN(value: boolean) {
    this.inLogin = value;
  }
  @Mutation
  SET_USER_DATA(value: UserType) {
    localStorage.setItem("admin", JSON.stringify(value));
    this.user = value;
  }
  @Mutation
  SET_USER_TOKENS(value: { accessToken: string; refreshToken: string }) {
    localStorage.setItem("adminTokens", JSON.stringify(value));
    this.userTokens = value;
  }
  @Mutation
  CLEAR_USER_DATA() {
    localStorage.removeItem("admin");
    localStorage.removeItem("adminTokens");
    location.reload();
  }

  @Action({ commit: "CLEAR_USER_DATA" })
  logout() {
    return;
  }

  get isLogged() {
    return !!this.user;
  }

  get userRole() {
    return this.user ? this.user.role : "";
  }
}

export default getModule(User);
